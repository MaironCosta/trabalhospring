package br.com.rtp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.rtp.entity.Teste;
import br.com.rtp.model.TesteModel;

@Controller
public class HelloWorldController {
		
	private TesteModel testeModel;
	
	public HelloWorldController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public HelloWorldController(TesteModel testeModel) {
		// TODO Auto-generated constructor stub
		this.testeModel = testeModel;
	}

	@RequestMapping(method=RequestMethod.GET, value = "/helloWorld/inicio")
	public String inicio () {		

		System.out.println("Executando a l�gica com Spring MVC - inicio");
		
		return "helloWorld/inicio";
		
	}

	@RequestMapping(method=RequestMethod.POST, value = "/helloWorld/execute")
	public String execute(Teste teste) {
		
		System.out.println("Executando a l�gica com Spring MVC - POST");
		
		System.out.println(testeModel.execute(teste));
		
		ModelAndView view = new ModelAndView();
		view.addObject("teste", teste);
		
		return "helloWorld/helloWorld";
	}

}
